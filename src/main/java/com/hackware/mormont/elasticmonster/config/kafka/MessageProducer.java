package com.hackware.mormont.elasticmonster.config.kafka;

import com.hackware.mormont.elasticmonster.dto.NewProductRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;

public class MessageProducer {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    KafkaTemplate<String, String> producer;

    @Autowired
    KafkaTemplate<String, NewProductRequest> productProducer;
    @Value(value = "${message.topic.name}")
    private String topicName;

    public ListenableFuture<SendResult<String, NewProductRequest>> sendProductToTopic(NewProductRequest entity) {
        ListenableFuture<SendResult<String, NewProductRequest>> listenable = null;

        try {
            listenable = productProducer.send(topicName, entity);
            logger.info("listenable: " + listenable.isDone());
        } catch (Exception e) {
            logger.error("Something went wrong when sending entity to topic: " + e.getMessage());
        } catch (Throwable th) {
            logger.error("Something went wrong when sending entity to topic: " + th.getMessage());
        }
        return listenable;
    }

    public void sendMessageToTopic(String entity) {
        producer.send(topicName, entity);
    }
}
