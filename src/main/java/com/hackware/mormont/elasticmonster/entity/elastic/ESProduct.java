package com.hackware.mormont.elasticmonster.entity.elastic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "catalog", type = "product_type")
public class ESProduct {

    @Id
    private String id;

    private String nameRu;

    private String nameKz;

    private String nameQq;

    private String descriptionRu;

    private String descriptionKz;

    private String descriptionQq;

    private String organizationName;

    private String productCategory;

    private String status;


}

