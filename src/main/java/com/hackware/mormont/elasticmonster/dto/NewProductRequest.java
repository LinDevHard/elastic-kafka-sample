package com.hackware.mormont.elasticmonster.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewProductRequest {

    private String name;

    private String description;

    private String organizationName;

    private String productCategory;

    private String status;
}
