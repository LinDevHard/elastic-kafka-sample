package com.hackware.mormont.elasticmonster.dto;

import lombok.Data;

@Data
public class SearchRequest {
    String query;
}
