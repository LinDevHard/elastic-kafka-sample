package com.hackware.mormont.elasticmonster.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class KafkaMessageRequest {
    String message;
}
