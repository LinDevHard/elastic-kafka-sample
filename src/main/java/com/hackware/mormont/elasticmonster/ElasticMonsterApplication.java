package com.hackware.mormont.elasticmonster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@PropertySources({
        @PropertySource("application.properties"),
        @PropertySource("application-services.properties")
})
@SpringBootApplication
public class ElasticMonsterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticMonsterApplication.class, args);
    }

}
