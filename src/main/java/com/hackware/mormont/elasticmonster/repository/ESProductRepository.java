package com.hackware.mormont.elasticmonster.repository;

import com.hackware.mormont.elasticmonster.entity.elastic.ESProduct;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ESProductRepository extends ElasticsearchRepository<ESProduct, String> {

    List<ESProduct> findByDescriptionKzIn(Collection<String> query);
}
