package com.hackware.mormont.elasticmonster.controller;

import com.hackware.mormont.elasticmonster.config.kafka.MessageProducer;
import com.hackware.mormont.elasticmonster.dto.KafkaMessageRequest;
import com.hackware.mormont.elasticmonster.dto.NewProductRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaController {

    private final MessageProducer messageProducer;

    public KafkaController(MessageProducer messageProducer) {
        this.messageProducer = messageProducer;
    }

    @PostMapping("/send/message")
    public String message(@RequestBody KafkaMessageRequest messageRequest) {
        messageProducer.sendMessageToTopic(messageRequest.getMessage());
        return messageRequest.getMessage();
    }

    @PostMapping("/product/to/topic")
    public NewProductRequest addProduct(@RequestBody NewProductRequest request) {
        messageProducer.sendProductToTopic(request);
        return request;
    }
}
