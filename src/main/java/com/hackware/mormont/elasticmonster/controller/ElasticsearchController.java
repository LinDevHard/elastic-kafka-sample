package com.hackware.mormont.elasticmonster.controller;

import com.hackware.mormont.elasticmonster.dto.NewProductRequest;
import com.hackware.mormont.elasticmonster.dto.SearchRequest;
import com.hackware.mormont.elasticmonster.entity.elastic.ESProduct;
import com.hackware.mormont.elasticmonster.services.elastic.ESProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class ElasticsearchController {

    @Autowired
    ESProductService esProductService;

    @PostMapping("/product/add")
    public ESProduct addProduct(@RequestBody NewProductRequest request) {
        ESProduct esProduct = new ESProduct();
        esProduct.setDescriptionRu(request.getDescription());
        esProduct.setDescriptionKz(request.getDescription());
        esProduct.setDescriptionQq(request.getDescription());
        esProduct.setNameKz(request.getName());
        esProduct.setNameRu(request.getName());
        esProduct.setNameQq(request.getName());
        esProduct.setOrganizationName(request.getOrganizationName());
        esProduct.setProductCategory(request.getProductCategory());
        esProduct.setStatus(request.getStatus());
        return esProductService.save(esProduct);
    }

    @PostMapping("/product/search")
    public List<ESProduct> addProduct(@RequestBody SearchRequest request) {
        return esProductService.findBySearchQuery(Arrays.asList(request.getQuery().split(" ")));
    }

}
