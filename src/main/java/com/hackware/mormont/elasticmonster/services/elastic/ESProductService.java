package com.hackware.mormont.elasticmonster.services.elastic;

import com.hackware.mormont.elasticmonster.entity.elastic.ESProduct;

import java.util.List;

public interface ESProductService {

    ESProduct save(ESProduct entity);

    List<ESProduct> findBySearchQuery(List<String> query);

    void delete(ESProduct entity);
}
