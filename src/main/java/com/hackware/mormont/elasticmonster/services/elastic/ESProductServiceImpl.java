package com.hackware.mormont.elasticmonster.services.elastic;

import com.hackware.mormont.elasticmonster.entity.elastic.ESProduct;
import com.hackware.mormont.elasticmonster.repository.ESProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ESProductServiceImpl implements ESProductService {
    private final ESProductRepository repository;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public ESProductServiceImpl(ESProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public ESProduct save(ESProduct entity) {
        ESProduct saved = null;

        try {
            saved = repository.save(entity);
        } catch (Exception e) {
            logger.error("Ups! " + e.getMessage());
        }
        return saved;
    }

    @Override
    public List<ESProduct> findBySearchQuery(List<String> query) {
        return repository.findByDescriptionKzIn(query);
    }

    @Override
    public void delete(ESProduct entity) {
        repository.delete(entity);
    }
}
